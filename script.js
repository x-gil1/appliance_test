// ===== VARIABLES GÉNÉRALES ===== //

var square_list = [];
var clicked_squares = [];

const body = document.getElementById("body");
const area_button = document.createElement("button");
area_button.classList.add("button");
area_button.innerHTML = "Closest area";
area_button.onclick = showClosestArea;
body.appendChild(area_button);

/**
 * @returns un integer compris entre 0 et 255
 */
function randomRGB() {
  return Math.floor(Math.random() * 256);
}

// ===== CRÉATION DES RECTANGLES ===== //

function createRectangle(createEvent) {
  // On crée un element de DOM j'utilise ici une div
  // On lui donne les propriétés nécéssaires pour qu'il puisse être placé n'importe où sur la page
  // On lui donne sa position par rapport au clique de la souris. Ce sera en haut à gauche
  // On lui donne une couleur random, ici j'utilise les RGB donc il aura un rouge, un bleu et un vert aléatoire entre 0 et 255
  // On lui donne un id, contenant une indication sur sa place dans la liste des carrés ajoutés ( square_list )
  // Et on attache l'élément au DOM
  const square = document.createElement("div");
  square.style.position = "fixed";
  square.style.top = `${createEvent.clientY}px`;
  square.style.left = `${createEvent.clientX}px`;
  square.style.backgroundColor = `rgb(${randomRGB()}, ${randomRGB()}, ${randomRGB()})`;
  square.id = `square-${square_list.length}`;

  body.appendChild(square);

  /**
   * Compare la position X et Y initiale du clique avec celle de position actuelle de la souris
   * et définit par soustraction la taille du carré.
   * @param {event} drawEvent
   */
  function draw(drawEvent) {
    square.style.width = `${drawEvent.clientX - createEvent.clientX}px`;
    square.style.height = `${drawEvent.clientY - createEvent.clientY}px`;
  }

  /**
   * Enlève deux events listeners, celui du tracage et lui-même.
   * Ajoute le carré dans la liste des carrés ajoutés ( square_list )
   */
  function endDrawing() {
    // Si l'élément à une air de 0, il est supprimé du DOM
    if (square.offsetHeight === 0 || square.offsetWidth === 0) {
      body.removeChild(square);
    } else {
      square_list.push({
        element: square,
        area: square.offsetHeight * square.offsetWidth,
      });
    }
    window.removeEventListener("mousemove", draw);
    window.removeEventListener("mouseup", endDrawing);
  }

  window.addEventListener("mousemove", draw);
  window.addEventListener("mouseup", endDrawing);
}

// ===== ROTATION ET SUPPRESSION DES RECTANGLES ===== //

/**
 * removeSquares passe sur chacun des éléments cliqués les enlèves du DOM ainsi que de la liste des éléments créés
 */
function removeSquares() {
  clicked_squares.forEach((clicked_square) => {
    square_list = square_list.filter(
      (square) => square.element.id !== clicked_square.id
    );
    body.removeChild(clicked_square);
  });
}

/**
 * Cette fonction prends en paramètre une div du DOM, un square préalablement dessiné.
 * Elle lui ajoute la classe rotate, contenant une animation de une seconde faisant tourner l'objet de 360 degrés.
 * Une fois la seconde passé, la fonction regarde si l'élément correspond au dernier élément cliqué, contenu au dernier index de clicked_squares.
 * Si c'est le cas, on ira supprimer tous les éléments cliqués du DOM ainsi que de l'array des éléments cliqués ( clicked_squares ).
 * @param {DOM} target
 */
function rotate_square(target) {
  target.classList.add("rotate");
  setTimeout(() => {
    if (clicked_squares[clicked_squares.length - 1] === target) {
      removeSquares();
      clicked_squares = [];
    }
  }, 1000);
}

// ===== MISE EN ÉVIDENCE DES AIRES LES PLUS PROCHES ===== //

/**
 * Passe sur chacun des rectangles ajoutés au dom, les trie par taille et compare leurs aires
 * Applique une couleur de fond random aux éléments selectionnés
 */
function showClosestArea() {
  // On sort de la fonction si il n'y a pas assez de rectangle
  if (square_list.length < 2) {
    alert("Aucun rectangle à comparer");
    return;
  }
  // On liste les éléments par aires ( du plus petit au plus grand )
  // Cela permet de les comparer plus rapidement
  const sorted_squares = square_list.sort((a, b) => {
    return a.area - b.area;
  });

  // On instancie la couleur de fond à appliquer aux éléments
  const backgroundColor = `rgb(${randomRGB()}, ${randomRGB()}, ${randomRGB()})`;

  // On instancie la première option du résultat, en comparant les deux plus petis éléments
  var result = {
    square_a: sorted_squares[0].element,
    square_b: sorted_squares[1].element,
    difference: sorted_squares[1].area - sorted_squares[0].area,
  };

  // On loop sur chacun des éléments jusqu'au dernier et on compare les aires (n && n-1) avec l'air stocké dans result
  // on sauvegarde les éléments si l'air est plus petite
  // si l'air est équivalente, on compare l'id des éléments pour savoir le quel à été crée en premier et c'est cette pair que l'on mettra en avant dans l'exercice
  for (i = 2; i < sorted_squares.length; i++) {
    current_difference = sorted_squares[i].area - sorted_squares[i - 1].area;
    if (current_difference < result.difference) {
      result.square_a = sorted_squares[i - 1].element;
      result.square_b = sorted_squares[i].element;
      result.difference = current_difference;
    }
  }
  // Une fois la diffrence mimimale trouvé, on applique la couleur définie pour le background sur les éléments sélectionnés
  result.square_a.style.backgroundColor = backgroundColor;
  result.square_b.style.backgroundColor = backgroundColor;
}

function main() {
  // Ici on écoute pour pouvoir dessiner un carré. Si la souris descend, la fonction draw va s'activer
  body.addEventListener("mousedown", (event) => {
    createRectangle(event);
  });
  // Ici on écoute les doubles clicks. Si la cible double cliqué est un carré de la liste,
  // il sera immédiatement ajouté à la liste des carrés cliqués et on le fera passer dans la fonction de rotation
  body.addEventListener("dblclick", (event) => {
    if (event.target.id.includes("square-")) {
      clicked_squares.push(event.target);
      rotate_square(event.target);
    }
  });
}

main();
